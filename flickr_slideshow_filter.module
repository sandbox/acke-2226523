<?php
/**
 * @file
 * Provides integration of jQuery Flickr Slideshows through an input filter
 *
 * This module provides an input filter to add a slideshow from Flickr Photoset anywhere
 * input filters are accepted.
 */

/**
 * Implements hook_help().
 */
function flickr_slideshow_filter_help($path, $arg) {
  switch ($path) {
    case 'admin/help#flickr_slideshow_filter':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Flickr Slideshow Input filter makes it possible to embed flickr photoset jquery slideshows everywhere input filters are accepted. If you want tooltips, install the module qtip and use the selector <i>.flickr_slideshow a</i></p>');
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Usage') . '</dt>';
      $output .= '<dd>' . t('To embed a slideshow, you need to use the following code snippet: <samp>[flickr_slideshow id=photoset_id]</samp>.') . '.</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implementation of hook_filter_info().
 */
function flickr_slideshow_filter_filter_info() {
  $filters['flickr_slideshow_filter'] = array(
    'title' => t('Flickr Slideshow filter'),
    'description' => t('Substitutes [flickr_slideshow id=xxx] tags with a flickr photoset slideshow'),
    'process callback'  => 'flickr_slideshow_filter_filter_process',
    'settings callback' => 'flickr_slideshow_filter_filter_settings',
    'tips callback' => 'flickr_slideshow_filter_filter_tips',
    'cache' => FALSE,
  );
  return $filters;
}

/**
 * Implementation of hook_init
 */

function flickr_slideshow_filter_init() {
  if (module_exists('libraries')) {
      $cycle_lib = libraries_get_path('jquery.cycle');
      drupal_add_js($cycle_lib . '/jquery.cycle.all.js', array('group' => JS_LIBRARY));
  }
  else {
    drupal_add_js('http://ajax.microsoft.com/ajax/jquery.cycle/2.88/jquery.cycle.all.js');
  }

/*
    drupal_add_js(
      array(
        'flickr_slideshow_filter' => array(
          'flickr_slideshow_filter_speed' => $filter->settings['flickr_slideshow_filter_speed'],
          'flickr_slideshow_filter_timeout' => $filter->settings['flickr_slideshow_filter_timeout'],
        )
      ),
      'setting'
    );
*/
  drupal_add_js(drupal_get_path('module', 'flickr_slideshow_filter') . '/js/flickr_slideshow_filter.js');
  drupal_add_css(drupal_get_path('module', 'flickr_slideshow_filter') . '/css/flickr_slideshow_filter.css');
  if (module_exists('qtip')) {
    drupal_add_library('qtip', 'qtip');
  }
}

/**
 * Process callback for hook_filter_info().
 */
   
function flickr_slideshow_filter_filter_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  if (preg_match('/\[flickr_slideshow id=([^\]]*)\]/', $text, $match)) {
    $photoset_id = $match[1];   
    module_load_include('module', 'flickrapi');
    $flickr = flickrapi_phpFlickr();
    if (!$flickr) {
      drupal_set_message(t("Unable to query flickr.com, library is missing."), 'error');
      return FALSE;
    }
    $photos = $flickr->photosets_getPhotos($photoset_id);
    //drupal_set_message('<pre>'. print_r($flickr, TRUE) .'</pre>');
    $html = '<div class="flickr_slideshow">';
    foreach ($photos['photoset']['photo'] as $photo) { 
      $html .= "<a href='".$flickr->buildPhotoURL($photo, "medium")."' title='".$photo['title']."'>";
      $html .= "<img alt='".$photo['title']."' src='".$flickr->buildPhotoURL($photo, "medium")."'></a>";
    }
      $html .= '</div>';
    $replacement = '<div class="flickr_slideshow"></div>';
    $text = str_replace($match[0], $html, $text);
  }
  return $text;
}

/**
 * Settings callback for flickr_slideshow_filter
 */
function flickr_slideshow_filter_filter_settings($form, $form_state, $filter, $format, $defaults) {
  $settings['flickr_slideshow_filter_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('jQuery Cycle Slideshow speed'),
    '#required' => false,
    '#size' => 5,
    '#default_value' => !empty($filter->settings['flickr_slideshow_filter_speed']) ? $filter->settings['flickr_slideshow_filter_speed'] : '1000',
  );
  $settings['flickr_slideshow_filter_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('jQuery Cycle Slideshow timeout'),
    '#required' => false,
    '#size' => 5,
    '#default_value' => !empty($filter->settings['flickr_slideshow_filter_timeout']) ? $filter->settings['flickr_slideshow_filter_timeout'] : '3000',
  );
  $settings['flickr_slideshow_filter_caption'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Caption'),
    '#description' => t('Title is used for caption. Depends on qTip module.'),
    '#default_value' => !empty($filter->settings['flickr_slideshow_filter_caption']) ? $filter->settings['flickr_slideshow_filter_caption'] : 1,
  );
  $settings['flickr_slideshow_filter_pause'] = array(
    '#type' => 'checkbox',
    '#title' => t('jQuery Cycle Slideshow Pause on hover'),
    '#default_value' => !empty($filter->settings['flickr_slideshow_filter_pause']) ? $filter->settings['flickr_slideshow_filter_pause'] : 1,
  );
  $settings['flickr_slideshow_filter_random'] = array(
    '#type' => 'checkbox',
    '#title' => t('jQuery Cycle Slideshow Random order on images'),
    '#default_value' => !empty($filter->settings['flickr_slideshow_filter_random']) ? $filter->settings['flickr_slideshow_filter_random'] : 1,
  );
  return $settings;
}

/**
 * Implements hook_filter_tips().
 */
function flickr_slideshow_filter_filter_tips($filter, $format, $long = FALSE) {
  $action = t('embed the flickr slideshow');
  return t('Use [flickr_slideshow id=xx]');
}

(function($) {
	Drupal.behaviors.flickr_slideshow_filter = {
		attach: function(context, settings) {
      if ($('.flickr_slideshow').length) {
        $('.flickr_slideshow').cycle({
      	  fx:       'fade',
          speed:    1000,
          timeout:  3000,
          next:     '.flickr_slideshow',
          pause:    1,
          random:   true
        });
      } else {
        // do nothing 
      }
		}
	};
})(jQuery);